function transformTypedCharacter(typedChar) {
    return typedChar.toLowerCase();
}

function insertTextAtCursor(text) {
    var sel, range, textNode;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0).cloneRange();
            range.deleteContents();
            textNode = document.createTextNode(text);
            range.insertNode(textNode);

            // Move caret to the end of the newly inserted text node
            range.setStart(textNode, textNode.length);
            range.setEnd(textNode, textNode.length);
            sel.removeAllRanges();
            sel.addRange(range);
        }
    } else if (document.selection && document.selection.createRange) {
        range = document.selection.createRange();
        range.pasteHTML(text);
    }
}


function lower(evt) {
    if ((evt.key.charCodeAt(0) >= 65) && (evt.key.charCodeAt(0) <= 90)) {
        console.log('*****BEFORE******');
        console.log(evt.keyCode);
        console.log(evt.which);
        console.log(evt.key);
        console.log('***********');
        var newChar = evt.key.toLowerCase();
        evt.preventDefault()
        var newEvent = $.Event("keyup");
        newEvent.ctrlKey = false;
        newEvent.keyCode = newChar.charCodeAt(0);
        newEvent.which = newChar.charCodeAt(0);
        newEvent.key = newChar;
        newEvent.returnValue = true;
        $(evt.target).trigger(newEvent);

        console.log('#######AFTER##########');
        console.log(newEvent.keyCode);
        console.log(newEvent.which);
        console.log(newEvent.key);
        console.log('#################');
    }

};