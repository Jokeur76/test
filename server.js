var express = require('express');
var session = require('express-session');
var bodyParser = require("body-parser");
//var http = require('http');
//var jsdom = require('jsdom');
//var fs = require("fs");
//var Rx = require("rxjs/Rx");
//var chalk = require('chalk');
//var oauth = require('oauth');
//var util = require('util');
//var readline = require('readline')

var app = express();

app.use(session({secret: "test"}));

app.use(express.static('public'));

//for content type application/json
app.use(bodyParser.json());

app.use('/scripts', express.static(__dirname + '/node_modules'));


app.get('/', function (req, res) {
    res.sendFile(__dirname + "/" + "test.html");
});

var server = app.listen(5000, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);
});